#include "searchDictionary.h"

std::string textToMorse (const std::string &untranslatedString)
{
  std::string translatedString;
  for (auto &character : untranslatedString)
    {
      if (!whitespace (character))
        {
          translatedString += searchDictionary (character) + " ";
        }
      else
        {
          translatedString += character;
        }
    }
  return translatedString;
}
