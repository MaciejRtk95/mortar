bool whitespace (char character)
{
  return character == ' ' || character == '\t' || character == '\n';
}
