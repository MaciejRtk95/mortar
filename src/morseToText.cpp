#include "getCode.h"

std::string morseToText (const std::string &morseCode)
{
  std::string translatedText;
  for (unsigned i = 0; i < morseCode.length (); i++)
    {
      if (!whitespace (morseCode[i]))
        {
          translatedText += searchDictionary (getCode (morseCode, i));

          //Avoiding screwing up formatting
          if (whitespace (morseCode[i]) && morseCode[i] != ' ' && i < morseCode.length ())
            {
              translatedText += morseCode[i];
            }
        }
      else
        {
          translatedText += morseCode[i];
        }
    }

  return translatedText;
}
